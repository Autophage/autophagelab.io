export default [
  {
    title: 'Renai Circulation',
    filename: 'renai.mp3',
    duration: 76.128,
    description: "My first MIDI song to include drums. I wrote this after finding out that even my girlfriend, who had never watched a minute of anime in her life, was well-acquainted with it. It was very easy - all the tracks are simple to pick out in the original, and there's very little variation in the drums. The only crummy part is converting rapping into MIDI, but I think I did a pretty decent job of it."
  },
  {
    title: 'Dorohedoro Opening',
    filename: 'doro.mp3',
    duration: 71.42515,
    description: "The opening to Dorohedoro is one of my favorite anime openings of all time, and a big part of that is just because it's not too far off from the music I normally listen to. Lots of fun, crazy instruments in this one, I don't have much else to say about it. I did cut out the very end so that it would finish with the solo, which I think is much cooler."
  },
  {
    title: 'Attack on Titan Opening 5',
    filename: 'op5.mp3',
    duration: 87.744,
    description: "My first anime cover song, and what a one to start with! There are a lot of tracks in the original, and many of them are buried pretty far, I did my best with most of them and ignored some others. Season 3 Part 2 of Attack on Titan was just too good for me not to have done it, though. The instruments are from the Kirby games, some from Super Star and some from 64. I like covering anime openings because they're full songs edited down to 90 seconds, which is about how long the average song I write lasts anyways."
  },
  {
    title: 'The All-Seeing Eye',
    filename: 'eye.mp3',
    duration: 137.036012,
    description: ''
  },
  {
    title: 'Dead Selves Strewn',
    filename: 'dead.mp3',
    duration: 67.8,
    description: 'A very pared-down metal song, which includes my very first drum programming. I think I wrote this song at a time when I had a lot of hand pain, and had to give up gaming and most of my guitar playing. Being surrounded by my hobbies but unable to engage in them, it felt like they were little parts of me that had died and were lying around my room, which is where the song title came from. This song is essentially just 1 riff for its entire duration, and it was very fun and easy to write and record. I could see some day in the future where I maybe make an album of 10 of these in a single day.'
  },
  {
    title: 'Extraterrestrial Megaflora',
    filename: 'megaflora.mp3',
    duration: 77.268876,
    description: 'The first MIDI song I wrote with the intention of it only ever being a MIDI song, not something to record on guitar later. This is one of my personal favorites, which of course is not the case for most people due to that grating diminished 5th in the middle point. I think part of why I like it so much is because it was supposed to be very conceptual, about a big ship in space that crash lands on an alien planet. The opening notes are like twinkling stars, while the bass and melody represent the ship lazily making its way through the ether, almost rising and falling like a ship on the ocean. The diminished 5th is the alarm that signals disaster, and what follows next is the crazy sights and sounds of the planet that the crew eventually finds themselves stranded on. I think this is the best example of my songwriting tendencies to have 2-sections and end with a lot of momentum.'
  },
  {
    title: 'Cedars',
    filename: 'cedar.mp3',
    duration: 76.302362,
    description: "The full title of this song is 'The Scent of Rain-Kissed Cedars', which was inspired by my post-rainfall walks. However, as it is now, it's just a song fragment, so it gets just a fragment of its full title. For this song I came up with all the chords and then worked on the melody over top that. The next section of the song was meant to have a lot more 'metal' to it, though I really don't remember my plan for it very well at all. With the exception of the very end, this is definitely the best job I ever did of recording a song, in the playing, guitar tone, and mixing."
  },
  {
    title: 'Hopeless Penitent | Victim Intransigent',
    filename: 'hopeless.mp3',
    duration: 82.056,
    description: "This is another instance of my 2-section songwriting - it has 'hopeless' and 'intransigent' halves. I think I came up with the name first, which informed the concept behind the song structure, but I'm not totally sure. The first half I wrote on guitar before transposing to MIDI, while the second part was done entirely on MIDI. I don't want to get too much into the inspiration of this song, as it's emotional, which makes it quite lame to pontificate about, but it is about a specific person, who also appears a couple times in my drawings..."
  },
  {
    title: 'Verdant Fields',
    filename: 'verdant.mp3',
    duration: 86.088,
    description: "This was meant to be song 1 of a 4-song EP I was going to write centered on the world of Adventure Time. The other songs would be about Princess Bubblegum (Which I wrote as a MIDI but never recorded on guitar), the Ice King (A guitar cover of the song 'Frost Mage' by Khand, of which I recorded half), and Marceline (Which I barely got started on at all). Ultimately, only this song was complete enough to show the world. This song was about Finn and Jake having a lazy day outside of their treehouse. My approach to this one was to take the chords in the opening song and arpeggiate them, so that they wouldn't be instantly recognizable. I played in a rhythm similar to the guitar track of the 'Lon Lon Ranch' theme from the Zelda 64 games, as I found the song to be very evocative of a lazy, carefree day in the countryside. By the end, I bring in the melody from the opening theme and strum the opening chords instead of arpeggiating them, which hopefully makes the link to the Adventure Time theme throughout the song clear."
  },
  {
    title: 'Everything Stays',
    filename: 'stays.mp3',
    duration: 44.64,
    description: "This is a cover of the best song from Adventure Time. I performed this cover live alongside my friend in China at a summer camp we volunteered at, which is something I think I'll always remember."
  },
  {
    title: 'Gravity Falls Intro',
    filename: 'gravity.mp3',
    duration: 43.36252,
    description: "Gravity Falls' intro has a real punk-ish chord progression, and when I realized that I had to make a cover. This song was exceedingly easy, which is balanced out by the really difficult solo at the beginning. I didn't quite nail it, and I'm pretty sure I did tens, if not hundreds, of takes to do it even that well. Near the very end I add to the power chords by playing perfect 4ths on the E string, which I think really helps add to the momentum. I think this was the first instance of my tendency to make a song as intense as possible right when it ends. I'm not too happy with the clean guitar right at the end - I should have separated the guitars into either channel, as it is now it all blends together. I never tried too hard when recording, but if I had I think it could have been really cool if I'd played the melody backwards, then reversed the melody to give it a more off-kilter feel."
  },
  {
    title: 'Spectral Decomposition',
    filename: 'spectral.mp3',
    duration: 140.664,
    description: "This was the first song I wrote and one of the first I ever recorded. I got the title from a concept I was introduced to in a physics class, which I immediately thought was the perfect title for a death/doom song. Like many of the songs I write, there are 2 sections - in this case, I wanted the beginning to sound dense, and the ending to be much more enveloping - this second half would be the 'Decomposition' of the first (In a very general sense, I don't believe any motifs were shared between the two halves). I recall that I wrote the rhythm sections on guitar, then looped them while I figured out the melodies to write over them. There was obviously a lot of imperfection in the performance (even by my standards), but I actually feel that all the little flaws added to it in positive ways (particularly the noise from my guitar that comes in at 1:11 before it starts playing)."
  }
]
